package com.example.nim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class PlayActivity extends Activity {

	private TextView potez;
	private TextView g1;
	private TextView g2;
	private TextView g3;
	private EditText nmb;
	int gomila;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		potez = (TextView) findViewById(R.id.potez1);
		g1 = (TextView) findViewById(R.id.g1);
		g2 = (TextView) findViewById(R.id.g2);
		g3 = (TextView) findViewById(R.id.g3);
		Intent intent = getIntent();
		potez.setText(intent.getStringExtra("prvi"));
		g1.setText(intent.getStringExtra("g1"));
		g2.setText(intent.getStringExtra("g2"));
		g3.setText(intent.getStringExtra("g3"));
		nmb = (EditText) findViewById(R.id.nmb);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.newgame) {
			nova();
			return true;
		}else if(id==R.id.endgame){
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	public void izaberiGomilu(View view) {

		boolean checked = ((RadioButton) view).isChecked();

		switch (view.getId()) {
		case R.id.tg1:
			if (checked)
				gomila = 1;
			break;
		case R.id.tg2:
			if (checked)
				gomila = 2;
			break;
		case R.id.tg3:
			if (checked)
				gomila = 3;
			break;
		}
	}

	public void igraj(View v) {
		if (gomila == 1) {
			if (Integer.parseInt(nmb.getText().toString()) <= Integer
					.parseInt(g1.getText().toString())) {
				int p = Integer.parseInt(g1.getText().toString())
						- Integer.parseInt(nmb.getText().toString());
				g1.setText(Integer.toString(p));
				proveri();
				if (potez.getText().toString()
						.equals(getString(R.string.txt_pera)))
					potez.setText(getString(R.string.txt_mika));
				else
					potez.setText(getString(R.string.txt_pera));
			} else {
				Toast.makeText(PlayActivity.this, getString(R.string.greska2),
						Toast.LENGTH_SHORT).show();
			}
		} else if (gomila == 2) {
			if (Integer.parseInt(nmb.getText().toString()) <= Integer
					.parseInt(g2.getText().toString())) {
				int p = Integer.parseInt(g2.getText().toString())
						- Integer.parseInt(nmb.getText().toString());
				g2.setText(Integer.toString(p));
				proveri();
				if (potez.getText().toString()
						.equals(getString(R.string.txt_pera)))
					potez.setText(getString(R.string.txt_mika));
				else
					potez.setText(getString(R.string.txt_pera));
			} else {
				Toast.makeText(PlayActivity.this, getString(R.string.greska2),
						Toast.LENGTH_SHORT).show();
			}
		} else if (gomila == 3) {
			if (Integer.parseInt(nmb.getText().toString()) <= Integer
					.parseInt(g3.getText().toString())) {
				int p = Integer.parseInt(g3.getText().toString())
						- Integer.parseInt(nmb.getText().toString());
				g3.setText(Integer.toString(p));
				proveri();
				if (potez.getText().toString()
						.equals(getString(R.string.txt_pera)))
					potez.setText(getString(R.string.txt_mika));
				else
					potez.setText(getString(R.string.txt_pera));
			} else {
				Toast.makeText(PlayActivity.this, getString(R.string.greska2),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void proveri() {
		int a, b, c;
		a = Integer.parseInt(g1.getText().toString());
		b = Integer.parseInt(g2.getText().toString());
		c = Integer.parseInt(g3.getText().toString());
		String p = getString(R.string.pobeda);
		p = p.concat(potez.getText().toString());
		if (a == 0 && b == 0 && c == 0)
			Toast.makeText(PlayActivity.this, p, Toast.LENGTH_SHORT).show();
	}
	
	public void nova(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
}
