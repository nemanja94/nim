package com.example.nim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NimActivity extends Activity {

	private TextView txtNaslov;
	private TextView txtPera;
	private TextView txtMika;
	private EditText edGomila1;
	private EditText edGomila2;
	private EditText edGomila3;
	private CheckBox chPrvi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nim);
		txtNaslov = (TextView) findViewById(R.id.title);
		txtPera = (TextView) findViewById(R.id.label_pera);
		txtMika = (TextView) findViewById(R.id.label_mika);
		edGomila1 = (EditText) findViewById(R.id.gomila1);
		edGomila2 = (EditText) findViewById(R.id.gomila2);
		edGomila3 = (EditText) findViewById(R.id.gomila3);
		chPrvi = (CheckBox) findViewById(R.id.igrac);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nim, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.fontUp) {
			povecaj();
			return true;
		}else if(id==R.id.fontDown){
			smanji();
			return true;
		}else if(id==R.id.end){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	/* Mnogo je mali broj 1 tako da se teze primeti promena, ako hocete podesite na 10 i videcete kako menja */
	
	public void povecaj(){
		txtNaslov.setTextSize(TypedValue.COMPLEX_UNIT_PX,txtNaslov.getTextSize()+1);
	}
	
	public void smanji(){
		txtNaslov.setTextSize(TypedValue.COMPLEX_UNIT_PX,txtNaslov.getTextSize()-1);;
	}
	
	public void startGame(View v){
		Intent intent = new Intent(this, PlayActivity.class);
		if(Integer.parseInt(edGomila1.getText().toString())>0 && Integer.parseInt(edGomila2.getText().toString())>0 &&Integer.parseInt(edGomila3.getText().toString())>0){
			intent.putExtra("g1", edGomila1.getText().toString());
			intent.putExtra("g2", edGomila2.getText().toString());
			intent.putExtra("g3", edGomila3.getText().toString());
			if(chPrvi.isChecked())
				intent.putExtra("prvi", getString(R.string.txt_pera));
			else
				intent.putExtra("prvi", getString(R.string.txt_mika));
			startActivity(intent);
		}else{
			Toast.makeText(NimActivity.this, R.string.greska1, Toast.LENGTH_SHORT).show();
		}
	}
}
